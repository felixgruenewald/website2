---
title: "Ideological and Affective Polarization in Multiparty Systems"
subtitle: Literature Review
authors: Arndt Leininger, Felix Grünewald, Nelly Buntfuß
date: 2024-05-22
publication: OSF Pre-Print
images: 
---

### About
Our team has decided to collect what we know about polarisation in multiparty systems in a review article that is now available on OSF. 

### Abstract
Inspired by increasingly partisan politics in the US, a burgeoning literatureon affective polarization applies the concept to multiparty systems. While there are many literature reviews on polarization in the US, as of yet there exists no comparable stock taking of comparative research on polarization in multi party systems. This review article fills this gap by providing a comprehensive overview of relevant concepts and operationalizations of ideological and affective polarization in multiparty systems and by summarizing comparative research on the causes and consequences of polarization in these systems. The review also makes an empirical contribution by creating and analyzing cross-national data on polarization to scope the relationship between ideological and affective polarization across several measures. We present the data and measures in an accompanying [Encyclopedia of Polarization](polarization.wiki).

<iframe src="https://mfr.osf.io/render?url=https://osf.io/download/64d606602fe4965c8e61b130/?direct%26mode=render"
    frameborder="0"
    scrolling="no" 
    style="overflow:hidden;height:800;width:100%" 
    height="800" 
    width="100%"></iframe>

[OSF Pre-Print](https://osf.io/preprints/socarxiv/mz6rs/)